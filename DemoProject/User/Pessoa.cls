Class User.Pessoa Extends %Persistent
{
property name As %String;
property age As %Numeric;
property child As %Numeric;
Storage Default
{
<Data name="PessoaDefaultData">
<Value name="1">
<Value>%%CLASSNAME</Value>
</Value>
<Value name="2">
<Value>name</Value>
</Value>
<Value name="3">
<Value>age</Value>
</Value>
<Value name="4">
<Value>child</Value>
</Value>
</Data>
<DataLocation>^User.PessoaD</DataLocation>
<DefaultData>PessoaDefaultData</DefaultData>
<IdLocation>^User.PessoaD</IdLocation>
<IndexLocation>^User.PessoaI</IndexLocation>
<StreamLocation>^User.PessoaS</StreamLocation>
<Type>%Library.CacheStorage</Type>
}

}